Files contain data from uboat.net exported to JSON and WKT (Well Known Text) formats. Some JSONs are adjusted for use with Hive (files with .hive.json extension). 

Files with names starting from uboats... contain data about German U-boats during WWII from pages linked here: https://uboat.net/boats/listing.html  
Details... files contain data about daily positions of uboats, like this one here: https://uboat.net/boats/patrols/details.php?boat=1&date=1940-03-15  
Men... files contain data about U-boats' commanders from pages linked here: https://uboat.net/men/commanders/  
Crews... files contain data about various intakes of officer cadets in the Kriegsmarine taken from pages linked here: https://uboat.net/men/commanders/crews.html  
lastPosition zipped file contains more than 1 000 WKT files with last positions of boats.  
details1939-1945.wkt are daily positions of U-boats during whole war (bear in mind that not all daily positions are known).

Most of the files have creation datetime embedded in their names.

These files where created as part of a thesis written in 2018 on Warsaw University of Technology Big Data studies. 

Feel free to use these files for your own research.